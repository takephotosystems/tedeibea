<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta charset="UTF-8">
<title>ギャラリー一覧｜写真館テディ・ベア 千葉県浦安市のフォトスタジオ</title>
<meta name="description" content="千葉県浦安市にある写真館テディ・ベアです。結婚式、七五三、成人式、お宮参り等の写真撮影をおこなってます。">
<meta name="keywords" content="テディベア,写真,フォトスタジオ,七五三,ウエディング,成人式,卒業写真,証明写真,千葉県,浦安市">


<!--Require Stylesheet-->
<link rel="stylesheet" href="../css/import.css">

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/fade.js"></script>
<script type="text/javascript" src="../js/slideshow.js"></script>
<script src="../js/scroll.js"></script>
<script type="text/javascript" src="../js/opacity-rollover2.1.js"></script>

		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->








<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />


<!-- ▽▽▽　埋め込み時以下head部にコピペ（lightbox、CSS等）ここから（lightboxフォルダも必須）　▽▽▽ -->

<link rel="stylesheet" type="text/css" href="lightbox/jquery.lightbox-0.5.css"/>
<style type="text/css">
/*--- CSSは設置ページ合わせて自由に編集ください --*/
/*---------------------------------
	        Base CSS 
---------------------------------*/
body,ul{ 
	margin:0;padding:0;list-style:none;
}
img{border:0}
/* clearfix(削除不可) */
.clearfix:after { content:"."; display:block; clear:both; height:0; visibility:hidden; }
.clearfix { display:inline-block; }
/* for macIE \*/
* html .clearfix { height:1%; }
.clearfix { display:block; }

#gallery_wrap {
	width:640px;
	margin:0 auto;
}
#gallery_list li{
	width:180px;
	height:150px;
	border:1px solid #ccc;
	float:left;
	margin:0 15px 10px 0;
	overflow:hidden;
	padding:5px;
	text-align:center;
	font-size:12px;
}
#gallery_list a.photo{
	width:180px;
	height:135px;
	margin:0 auto;
	overflow:hidden;
	display:block;
}
/*---------------------------------
	       /Base CSS 
---------------------------------*/

/*---------------------------------
	      Pager style
---------------------------------*/
.pager_link{
	text-align:right;
	padding:10px;
}
/*ページャーボタン*/
.pager_link a {
    border: 1px solid #aaa;
    border-radius: 5px 5px 5px 5px;
    color: #333;
    font-size: 12px;
    padding: 3px 7px 2px;
    text-decoration: none;
	margin:0 1px;
}
/*現在のページ、オーバーボタン*/
.pager_link a.current,.pager_link a:hover{
    background: #999;
    color: #FFFFFF;
}
.overPagerPattern{
	padding:0 2px ;	
}
/*---------------------------------
	      /Pager style
---------------------------------*/
</style>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>   
<script type="text/javascript" src="lightbox/jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#gallery_list a.photo').lightBox();//lightbox
});
</script>

<!-- △△△　埋め込み時head部にコピペ（lightbox、CSS等）ここまで　△△△ -->
</head>



<body onContextmenu="return false">

<!-- ▽▽▽　任意のページへ埋め込み時表示したい場所へコピペここから　▽▽▽ -->



<div id="frame">


<script type="text/javascript">
(function($) {
$(function() {

  $('.over1').opOver();
  $('#over2').wink();
  $('#over3').opOver(0.6,1.0);
  $('.over4').opOver(1.0,0.6,200,500);
  $('#over5').wink(200);
  $('#over6').wink('slow',0.2,1.0);
  $('#test1 .over ').opOver();

});
})(jQuery);

</script>










<header class="gallery2">

<div id="h_right">
<h1>写真館テディベアは千葉県浦安市にあるフォトスタジオです。</h1>
<img src="../images/freedial.png" alt="フリーダイヤル0120-81-2586">
<img src="../images/tel.png" alt="電話番号047-355-8201">
</div>

<div id="logo"><a href="http://www.shashinkan-teddybear.co.jp/index.htm"><img src="../images/logo.png" alt="写真館テディベア"></a></div>


<div class="clear"></div>


<nav>
<div>
<p class="nav_right"><a href="https://shashinkan-teddybear.co.jp/mailform.htm"><img class="over1" src="../images/nav_mail.png" alt="メールお問合せ"></a></p>
<p class="nav_right"><a href="https://login.photo-labo.jp/login/teddybear/" target="_blank"><img class="over1" src="../images/nav_photolabo.png" alt="フォトラボ"></a></p> 
</div>
<a href="http://www.shashinkan-teddybear.co.jp/index.htm"><img src="../images/nav_home.png" class="over1" alt="ホーム"></a><a href="http://www.shashinkan-teddybear.co.jp/index.htm#info"><img src="../images/nav_info.png" class="over1" alt="店舗案内・アクセス"></a><a href="http://ameblo.jp/ayunano19" target="_blank"><img src="../images/nav_blog.png" class="over1" alt="写真館テディベアのスタッフブログ"></a>

</nav>



<div class="clear"></div>

</header>






<div id="left_menu_box2">
<div id="menu2">
<p><img src="../images/menu/left_menu.png" alt="業務内容"></p>
<p class="test_g"></p>
<div id="menu_b2">
<p><a href="http://www.shashinkan-teddybear.co.jp/seijin.htm"><img src="../images/menu/m01seijin.png" alt="成人式" class="over1"></a></p>
<p><a href="http://www.shashinkan-teddybear.co.jp/wedding.htm"><img src="../images/menu/left_menu02.png" alt="ウエディング" class="over1"></a></p>
<p><a href="http://www.shashinkan-teddybear.co.jp/sotsugyou.htm"><img src="../images/menu/left_menu03.png" alt="卒業袴" class="over1"></a></p>
<p><a href="http://www.shashinkan-teddybear.co.jp/753.htm"><img src="../images/menu/left_menu04.png" alt="七五三" class="over1"></a></p>
<p><a href="http://www.shashinkan-teddybear.co.jp/omiya.htm"><img src="../images/menu/left_menu05.png" alt="お宮参り・百日記念" class="over1"></a></p>
<p><a href="http://www.shashinkan-teddybear.co.jp/sekku.htm"><img src="../images/menu/left_menu06.png" alt="初節句" class="over1"></a></p>

<p><a href="http://www.shashinkan-teddybear.co.jp/nyuugaku.htm"><img src="../images/menu/left_menu07.png" alt="入園入学・卒園卒業" class="over1"></a></p>

<p><a href="http://www.shashinkan-teddybear.co.jp/maternity.htm"><img src="../images/menu/left_menu08.png" alt="マタニティフォト" class="over1"></a></p>

<p><a href="http://www.shashinkan-teddybear.co.jp/family.htm"><img src="../images/menu/left_menu09.png" alt="家族写真" class="over1"></a></p>

<p><a href="http://www.shashinkan-teddybear.co.jp/silver.htm"><img src="../images/menu/left_menu10.png" alt="シルバーフォト" class="over1"></a></p>

<p><a href="http://www.shashinkan-teddybear.co.jp/juken.htm"><img src="../images/menu/left_menu11.png" alt="お受験写真" class="over1"></a></p>

<p><a href="http://www.shashinkan-teddybear.co.jp/idphoto.htm"><img src="../images/menu/left_menu12.png" alt="証明写真" class="over1"></a></p>
</div><!--menu_b-->

</div><!--menu-->

<p class="left_staff_blog"><a href="http://ameblo.jp/ayunano19" target="_blank"><img src="../images/menu/left_menu_blog.png" alt="スタッフブログ" class="over1"></a></p>
<p><a href="https://shashinkan-teddybear.co.jp/mailform.htm"><img src="../images/menu/left_menu_mail.png" alt="メールでのお問合せ" class="over1"></a></p>

</div><!--left_box-->





<div id="right_contents_box">

<h2><img src="../images/title/titlegallery.png" alt="成人式"></h2>





<p><a href="kabegami.html" target="_blank"><img src="download_b.png" alt="壁紙取得はこちらから" class="over1"></a></p>








<div id="gallery_wrap">
<?php
//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(START)
//----------------------------------------------------------------------
include_once("gallery/config.php");//設定ファイルインクルード
$img_updir = "gallery/upimg";//画像の保存先相対パス

//埋め込み設置するページの文字コード
//Shift-jisは「SJIS」、EUC-JPは「EUC-JP」と指定してください。デフォルトはUTF-8。
$encodingType = 'UTF-8';
//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(END)
//----------------------------------------------------------------------
	$lines = newsListSortUser(file($file_path),$copyright);//ファイル内容を取得
	if(!function_exists('PHPkoubou')){ echo $warningMesse; exit;}else{
	$pager = pagerOut($lines,$pagelength,$pagerDispLength);//ページャーを起動する
?>
<div class="pager_link"><?php echo $pager['pager_res'];?></div>
<ul id="gallery_list" class="clearfix">
<?php
for($i = $pager['index']; ($i-$pager['index']) < $pagelength; $i++){
  if(!empty($lines[$i])){
	$lines_array[$i] = explode(",",$lines[$i]);
	$lines_array[$i][3] = rtrim($lines_array[$i][3]);
	$lines_array[$i][1] = ymd2format($lines_array[$i][1]);//日付フォーマットの適用
	if($encodingType!='UTF-8') $lines_array[$i][1]=mb_convert_encoding($lines_array[$i][1],"$encodingType",'UTF-8');
	if($encodingType!='UTF-8') $lines_array[$i][2]=mb_convert_encoding($lines_array[$i][2],"$encodingType",'UTF-8');
	$alt_text = str_replace('<br />','',$lines_array[$i][2]);

//ギャラリー表示部（HTML部は自由に変更可）※デフォルトはサムネイルを表示。imgタグの「 thumb_ 」を取れば元画像を表示
echo <<<EOF
<li>{$lines_array[$i][1]} <a class="photo" href="{$img_updir}/{$lines_array[$i][0]}.{$lines_array[$i][3]}" title="{$lines_array[$i][1]}<br />{$lines_array[$i][2]}"><img src="{$img_updir}/thumb_{$lines_array[$i][0]}.{$lines_array[$i][3]}" alt="{$alt_text}" height="135" title="{$alt_text}" /></a>
<p class="detail_text">{$lines_array[$i][2]}</p>
</li>
EOF;
  }
}
?>
</ul>
<div class="pager_link"><?php echo $pager['pager_res'];?></div>
<?php PHPkoubou($encodingType,$copyright,$warningMesse);}//著作権表記削除不可?>
</div>
<!-- △△△　任意のページへ埋め込み時表示したい場所へコピペここまで　△△△ -->

</div><!--right_box-->



<div class="clear"></div>






<p class="gototop"><a href="scroll.js:scrollTo(0,0)" onClick="softScrollBack();return false;">▲このページのトップへ戻る</a></p>



<footer><small><img src="../images/copyright.png" alt="Copyright &copy; 写真館テディベア.All rights reserved."></small>
  
<p><strong>写真館テディベア</strong><br>
〒279-0043 千葉県浦安市富士見2-18-1<br>
フリーダイヤル／0120-81-2586<br>
TEL／047-355-8201<br>
営業時間／9：30～19：00　定休日／木曜日</p>

<p class="f_menu">
｜<a href="http://www.shashinkan-teddybear.co.jp/index.htm">HOME</a>｜<a href="http://www.shashinkan-teddybear.co.jp/index.htm#info">店舗案内・アクセス</a>｜<a href="http://ameblo.jp/ayunano19" target="_blank">スタッフブログ</a>｜<a href="https://shashinkan-teddybear.co.jp/mailform.htm">メールお問合せ</a>｜<a href="http://www.aget.co.jp/2.html" target="_blank">会社概要</a>｜<a href="http://www.aget.co.jp/7.html" target="_blank">個人情報保護方針</a>｜</p>

<p>｜<a href="http://www.shashinkan-teddybear.co.jp/seijin.htm">成人式</a>｜<a href="http://www.shashinkan-teddybear.co.jp/753.htm">七五三</a>｜<a href="http://www.shashinkan-teddybear.co.jp/nyuugaku.htm">入園入学・卒園卒業</a>｜<a href="http://www.shashinkan-teddybear.co.jp/silver.htm">シルバーフォト</a>｜<a href="http://www.shashinkan-teddybear.co.jp/wedding.htm">ウエディング</a>｜<a href="http://www.shashinkan-teddybear.co.jp/omiya.htm">お宮参り・百日記念</a>｜<a href="http://www.shashinkan-teddybear.co.jp/maternity.htm">マタニティフォト</a>｜<a href="http://www.shashinkan-teddybear.co.jp/juken.htm">お受験写真</a>｜<a href="http://www.shashinkan-teddybear.co.jp/sotsugyou.htm">卒業写真・卒業袴</a>｜<br>
｜<a href="http://www.shashinkan-teddybear.co.jp/sekku.htm">初節句</a>｜<a href="http://www.shashinkan-teddybear.co.jp/family.htm">家族写真</a>｜<a href="http://www.shashinkan-teddybear.co.jp/idphoto.htm">証明写真</a>｜</p>
  
<p>
<img src="../images/link.png" alt="→"><a href="http://agastia.jp/" target="_blank">アガスティア（市原）</a>　　<img src="../images/link.png" alt="→"><a href="http://agastia-photo.jp/" target="_blank">アガスティアフォト（市原）</a>　　<img src="../images/link.png" alt="→"><a href="http://www.m-soshakan.com/" target="_blank">武蔵野創寫舘（東川口）</a>　　<img src="../images/link.png" alt="→"><a href="http://www.studio-plus.jp/" target="_blank">STUDIO PLUS（高田馬場）</a>　　<img src="../images/link.png" alt="→"><a href="http://www.all-photostudio.com/" target="_blank">フォトスタジオ検索</a></p>

</footer>
</div>
<!--frame-->
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1496387-1";
urchinTracker();
</script>
</body>
</html>
