#!/usr/local/bin/perl
require "jcode.pl";
##
## 引数解析
#print "Content-type: text/html\n";
#print "\n";
#
&Com'getagv;
$PASSWD     = $Com'FORM{'passwd'};	# パスワード
$view = 'gallery/upimg/' . $PASSWD . '.jpg';
#$view = $PASSWD . '.jpg';
#$view = 'error.jpg';
##
if (-f $view) {
}
else {
    $view = 'error.jpg';
}
print "Content-type: image/jpeg\n\n";
open (IMGG,"$view");
binmode (IMGG);
print <IMGG>;
close (IMGG);
##
#print "PATH:$view\n";
##
exit(0);
## -------------------------------------------------------------------
## パッケージ・ライブラリ宣言
## -------------------------------------------------------------------
package Com;
## -------------------------------------------------------------------
## 引数解析
## -------------------------------------------------------------------
sub getagv {
	if ( $ENV{'CONTENT_TYPE'}  =~ m!^multipart/form-data!) { return 1; }
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	} else {
		$buffer = $ENV{'QUERY_STRING'};
	}
	@pairs = split(/&/, $buffer);
	foreach $pair (@pairs) {
		($name, $value) = split(/=/, $pair);
		$value =~ tr/+/ /;
		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		$value =~ s/<!--(.|\n)*-->//g;	#SSI 	除去(セキュリティ対策)
		$value =~ s/applet//gi;		#Java		除去(セキュリティ対策)
		$value =~ s/<script//gi;		#Javascript	除去(セキュリティ対策)
		$value =~ s/<META(.+)Refresh(.*)>(\s*)(\n?)//ig; #METAタグ飛ばし禁止
		$value =~ s/<(.*)style(\s*)=(.|\n)*>//ig;        #Styleタグ禁止
		&jcode'convert(*value, 'sjis');
		$FORM{$name} = $value;
#                print "$name = $value<br>\n";
	}
	return 0;
}
## -------------------------------------------------------------------
## ロック処理サブルーチン
## -------------------------------------------------------------------
sub lockon {
	local($lfile) = @_;
	$LCKKEY = 0;
	foreach (1 .. 5) {
		unless (-e "$lfile") {
			if ( open(LOCK,">$lfile") ) {
				close(LOCK);
				$LCKKEY = 9;
			}
			last;
		} else { sleep(1); }
	}
	if ($LCKKEY == 0) { return 0; }
	return 9;
}
## -------------------------------------------------------------------
## アンロック処理サブルーチン
## -------------------------------------------------------------------
sub lockoff {
	local($lfile,$key) = @_;
	if ($key == 9) { unlink("$lfile"); }
}
