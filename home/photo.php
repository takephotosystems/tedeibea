﻿<?php 
//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定
//----------------------------------------------------------------------
header("Content-Type:text/html;charset=utf-8");
include_once("./photo_cms/admin/include/config.php");//（必要に応じてパスは適宜変更下さい）
$img_updir = './photo_cms/upload';//画像保存パス（必要に応じてパスは適宜変更下さい）

session_start();

$authUser = 0;
$messe = '';
if(isset($_POST['data']['password']) && $_POST['token'] == '12sa65g4t46w5er84856w3e5r6typ8'){
	
//	//トークンチェック（CSRF対策）
//	if(empty($_SESSION['token']) || ($_SESSION['token'] !== $_POST['token'])){
//		exit('ページ遷移エラー(トークン)');
//	}
//	//トークン破棄
//	$_SESSION['token'] = '';
	
	
	$lines = file($file_path);
	foreach($lines as $val){
		$linesArr = explode(',',$val);
		if($linesArr[3] === $_POST['data']['password'] && $linesArr[7] == 1){
			$authUser = 1;
			break;
		}
	}
}

//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定
//----------------------------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>写真ページ</title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />

<style type="text/css">
/* CSSは必要最低限しか指定してませんのでお好みで（もちろん外部化OK） */
/* clearfix */
.clearfix:after { content:"."; display:block; clear:both; height:0; visibility:hidden; }
.clearfix { display:inline-block; }

/* for macIE \*/
* html .clearfix { height:1%; }
.clearfix { display:block; }

body{
	font-family:"メイリオ", Meiryo, Osaka, "ＭＳ Ｐゴシック", "MS PGothic", sans-serif;
	font-size:14px;
}
#photoWrap{
	margin:0;
	line-height:130%;
}
#photoWrap p{
	margin:5px 0;	
}
h2{
	font-size:16px;
	color:#369;
	margin:10px 0px 10px 0;
	font-weight:normal;
	border:1px solid #3D79B6;
	border-bottom:3px solid #3D79B6;
	padding:5px 10px;
	text-shadow:1px 1px 0px #fff;
}
#comment{
	border:1px solid #ccc;
	padding:10px;
}
#photoArea a{
	display:inline-block;
	margin:3px;
	padding:5px;
	border:1px solid #ccc;
}

</style>

</head>
<body>

<div id="photoWrap">

<h2>写真閲覧ページ</h2>

<?php if($authUser === 1){ //入力されたパスワードに一致したデータがある場合に該当データを表示する画面?>


<p id="name">お客様名：<?php echo h($linesArr[2]);?> 様</p>

<?php echo (!empty($linesArr[6])) ? '<p id="comment">'.$linesArr[6].'</p>' : '';//コメントが入力されていたら表示する?>

<div id="photoArea" class="clearfix">
<?php
for($i=0;$i<$maxCommentCount;$i++){
	
		//アップファイル表示用のタグをセット。 画像の場合はimgタグ、その他の場合はファイルにリンクする（タグ部分は自由に変更可）
		$upfileTag = '';//初期化
		foreach($extensionList as $val){
			$upFilePath = $img_updir.'/'.$linesArr[0].'-'.$i.'.'.$val;
			$upFilePathThumb = $img_updir.'/'.$linesArr[0].'-'.$i.'s.'.$val;
			if(file_exists($upFilePath)){
				$upfile_path = $upFilePath;
				$upfile_path_thumb = $upFilePathThumb;
				$upfileTag = '<a href="'.$upfile_path.'" target="_blank"><img src="'.$upfile_path.'?'.uniqid().'" width="140"></a>';//画像以外の場合のタグ
				break;
			}
		}
?>
<?php echo (!empty($upfileTag)) ? $upfileTag : '';?>
<?php } ?>
</div><!-- /photoArea -->



<?php }else{ //初めに表示するパスワード認証画面?>


<div id="passwordArea">
<?php echo (isset($_POST['data']['password'])) ? '<p style="color:red">入力されたパスワードが正しくないか、該当するデータが存在しません。</p>' : '';?>


<p>お店からお伝えしたパスワードを入力して送信ボタンを押して下さい。</p>



<form action="" method="post" id="authForm">
<?php
$token = sha1(uniqid(mt_rand(), true));
$_SESSION['token'] = $token;//トークンセット
?>
<input type="hidden" name="token" value="12sa65g4t46w5er84856w3e5r6typ8" />
<input type="text" name="data[password]" size="20" />
<input type="submit" name="submit" value="　送信　" id="submitBtn" />
</form>

<p>&nbsp;</p>
<p>&lt;写真の保存方法&gt;</p>
<p><strong>・スマートフォン</strong>　写真を長押しすると保存できます。</p>
<p><strong>・パソコン</strong>　写真の上で右クリック→「名前を付けて画像を保存」で保存できます。</p>


<p>&nbsp;</p>
<p>※このサービスは、ご注文時にホームページ・ギャラリーへの掲載をご許可いただいた方のみへのサービスとなっております。</p>
<p>※撮影からサイトへの掲載には若干のお時間をいただく場合がございます。</p>
<p>※ご不明な点はご遠慮なくお問合せください。</p>


</div><!-- /passwordArea -->

<?php } ?>




</div><!-- /photoWrap -->

</body>
</html>