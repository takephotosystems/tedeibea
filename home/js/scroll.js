var scrj = 1;
function softScrollBack() {
   if(navigator.appName == "Microsoft Internet Explorer" && document.compatMode == "CSS1Compat") {
      var scdist = document.body.parentNode.scrollTop;
   } else {
      var scdist = document.body.scrollTop;
   }
   if(scrj<50 && scdist) {
      scdist = (scdist>2) ? Math.ceil(scdist*.2) : 1;
      scrj++;
      scrollBy(0,-scdist);
      setTimeout("softScrollBack()",20);
   } else {
      scrollTo(0,0);
      scrj = 1;
   }
}