<?php
	require_once('./include/admin_inc.php');
	require_once('./include/config.php');
	require_once('./include/admin_function.php');
//----------------------------------------------------------------------
//  ログイン認証処理 (START)
//----------------------------------------------------------------------
	session_start();
	authAdmin($userid,$password);
//----------------------------------------------------------------------
//  ログイン認証処理 (END)
//----------------------------------------------------------------------


	//パスワード自動生成
	$id = getAutoIncreNum($file_path);
	$password = $id.'-'.generate_password(6);
	
	//念のためパスワードの重複をチェックする
	$chouhukuFlag = 0;
	$lines = file($file_path);
	foreach($lines as $val){
		$linesArr = explode(',',$val);
		if($linesArr[3] == $password){
			exit('パスワードの自動生成時に重複があったため、強制終了しました。<div id="toPage" class="linkBtn"><a href="./">こちらをクリックして登録をやり直して下さい。頻発するような場合にはシステム制作者にご連絡下さい。</a></div>');
			
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta name="robots" content="noindex,nofollow" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新規登録</title>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="./js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="./js/common.js"></script>

<script type="text/javascript">
$(function() {
	
	$(".validateForm").submit(function(){
		if($('input[name="data[title]"]').val() == ''){
			alert('お客様名を入力して下さい');
			return false;
		}else{
			return true;	
		}
	});
});
</script>
</head>
<body>
<div id="container" class="clearfix">
  <div id="logoutBtn" class="linkBtn"><a href="?logout=true">ログアウト</a></div>
  <div id="toPage" class="linkBtn"><a href="./">一覧へ</a></div>
  <h1>スタジオ写真 管理画面</h1>
  <h2>新規登録</h2>
  <form method="post" action="put.php" enctype="multipart/form-data" class="validateForm">
  
<?php
//トークンセット
$token = sha1(uniqid(mt_rand(), true));
$_SESSION['token'] = $token;
?>
<input type="hidden" name="token" value="<?php echo $token;//トークン発行?>" />
<input type="hidden" name="data[category]" value="" />
  
    <table class="borderTable01">
      <tr>
        <th>登録日</th>
        <td><?php echo registYmdList();//日付プルダウン表示?> </td>
      </tr>
      <tr>
        <th> 有効・無効</th>
        <td><input type="hidden" name="data[public_flag]" value="0" />
          <input type="checkbox" name="data[public_flag]" value="1" checked="checked" />
          （チェックで「有効」になります。無効にするとお客さんはページにアクセスできません）</td>
      </tr>
      <tr>
        <th>お客様名</th>
        <td><input type="text" size="40" name="data[title]" value="" /></td>
      </tr>
      <tr>
        <th>アクセス用パスワード<br />
        </th>
        <td>
          <input type="text" size="10" name="data[password]" value="<?php echo $password;?>" readonly="readonly" style="background:#FFF9DD;font-size:20px;padding:3px;" />
          （変更不可）
          <br />※すべて半角数字と半角のハイフンの組み合わせとなります。<br />
        ※ダウンロードページを開くのに必要ですのでお客さんにこのパスワードを伝えて下さい。<br />
        ※パスワードは重複しない値をシステム側で自動生成されます。そのため変更は不可となります。</td>
      </tr>
      
      
      <tr>
        <th>コメント、備考欄</th>
        <td><textarea cols="60" rows="3" name="data[comment]" class="mb10"></textarea><br />※入力した場合のみ表示されます。お客様にお伝えしたいことなどがあればご入力下さい。</td>
      </tr>
    </table>


<h3>写真ファイルアップロード</h3>

<p>※写真は自動で縮小、保存されます。（縦横比を維持したまま横写真は幅<?php echo $imgWidthHeight;?>px、縦写真は高さ<?php echo $imgWidthHeight;?>pxに）
※<?php echo $permissionExtension;?>のみ <br /> 
※一度に多数のファイルをアップした場合、失敗する可能性もあります。その場合、一旦登録後に「編集」にて複数回に分けてアップして下さい。
</p>

<table id="lineList" class="borderTable01">
<tbody>
<?php for($i=0;$i<$maxCommentCount;$i++){ ?>

      <tr class="lines1">
        <th>写真<?php echo $i+1;?>（2MB以内）</th>
        <td><input type="file" name="data[upfile][]" /></td>
      </tr>

<?php } ?>
</tbody>
</table>

<table class="borderTable01">
<tfoot> 
      <tr>
        <td colspan="2" align="center" valign="middle">アップロードと縮小処理を同時に行いますので、時間がかかることもありますが、そのままで待ってください。<br />
          <br />
          <input type="hidden" name="data[mode]" value="shinki" />
          <input type="submit" value="登録" class="submitBtn" /></td>
      </tr>
      
</tfoot>      
</table>
    <br />
  </form>
 <p class="pagetop linkBtn taR"><a href="#container">PAGE TOP▲</a></p>
</div>
</body>
</html>