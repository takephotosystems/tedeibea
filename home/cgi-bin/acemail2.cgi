#!/usr/local/bin/perl
require "jcode.pl";
require "aceconf2.cgi";
## -------------------------------------------------------------------
## メイン処理スタート
## -------------------------------------------------------------------
## 引数解析
print "Content-type: text/html\n";
print "\n";
#
&Com'getagv;
$NAME_K1    = $Com'FORM{'name_k1'};	# 漢字氏
$NAME_K2    = $Com'FORM{'name_k2'};	# 漢字名
$NAME_K     = $Com'FORM{'name_k'};	# 漢字氏名（メール出力用）
$NAME_F1    = $Com'FORM{'name_f1'};	# ふりがな氏
$NAME_F2    = $Com'FORM{'name_f2'};	# ふりがな名
$NAME_F     = $Com'FORM{'name_f'};	# ふりがな氏名（メール出力用）

$KNAME_K1    = $Com'FORM{'kname_k1'};	# 子供漢字氏
$KNAME_K2    = $Com'FORM{'kname_k2'};	# 子供漢字名
$KNAME_K     = $Com'FORM{'kname_k'};	# 子供漢字氏名（メール出力用）
$KNAME_F1    = $Com'FORM{'kname_f1'};	# 子供ふりがな氏
$KNAME_F2    = $Com'FORM{'kname_f2'};	# 子供ふりがな名
$KNAME_F     = $Com'FORM{'kname_f'};	# 子供ふりがな氏名（メール出力用）

$BIRTH_YYYY     = $Com'FORM{'birth_yyyy'};	# 生年月日（年）
$BIRTH_MM     = $Com'FORM{'birth_mm'};	# 生年月日（月）
$BIRTH_DD     = $Com'FORM{'birth_dd'};	# 生年月日（日）
$BIRTH_YMD     = $Com'FORM{'birth_ymd'};	# 生年月日（メール出力用）

$SEX        = $Com'FORM{'sex'};	        # 性別

$YUBIN        = $Com'FORM{'yubin'};	        # 郵便番号
$PREF        = $Com'FORM{'pref'};	        # 都道府県
$ADDR1     = $Com'FORM{'addr1'};	        # 住所１
$ADDR2     = $Com'FORM{'addr2'};	        # 住所２


$TEL        = $Com'FORM{'tel'};	        # 電話番号
$EMAIL      = $Com'FORM{'email'};	# メールアドレス
$EMAIL_K    = $Com'FORM{'email_k'};	# メールアドレス（確認用）

$MODE       = $Com'FORM{'MODE'};        # 確認ＯＫ = 'OK'

## 処理別に分ける
## print "MAIL：$mail_to<br>\n";
## -------------------------------------------------------------------
## メイン処理
## -------------------------------------------------------------------
## 処理別に分ける
if ( $MODE eq 'OK' )  {
                       &kakunin_set;
                       &output_mail(@USER_VALUE_LIST);
                       &user_message($THANKS_PAGE,\@DUMMY_LIST);
                      }
else {
    $err_f = 0;
    &input_check;
    if ( $err_f != 0 ){&user_message($FORMERR_PAGE,\@USER_ERROR_LIST);}
    else              {
                       &kakunin_set;
                       &user_message($CONFIRM_PAGE,\@USER_VALUE_LIST,\@USER_VALUE_LIST);
                      }
    }
exit(0);

## -------------------------------------------------------------------
## 入力チェックサブルーチン
## -------------------------------------------------------------------
sub input_check {
    if ( $NAME_K1 eq '' || $NAME_K2 eq '' ) {
        push(@USER_ERROR_LIST,$NAME_K_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if ( $NAME_F1 eq '' || $NAME_F2 eq '' ) {
        push(@USER_ERROR_LIST,$NAME_F_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if ( $KNAME_K1 eq '' || $KNAME_K2 eq '' ) {
        push(@USER_ERROR_LIST,$KNAME_K_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if ( $KNAME_F1 eq '' || $KNAME_F2 eq '' ) {
        push(@USER_ERROR_LIST,$KNAME_F_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if (  $BIRTH_YYYY eq '' || $BIRTH_MM eq '' || $BIRTH_DD eq '') {
        push(@USER_ERROR_LIST,$BIRTH_YMD_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if ( $SEX eq '' ) {
        push(@USER_ERROR_LIST,$SEX_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if (  $YUBIN eq '' || $PREF eq '' || $ADDR1 eq '') {
        push(@USER_ERROR_LIST,$ADDR_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }
    if ( $TEL eq '' ) {
        push(@USER_ERROR_LIST,$TEL_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
    }

    $mail_err = 0;
    if ( $EMAIL eq '' ) {
        push(@USER_ERROR_LIST,$EMAIL_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
        $mail_err = 1;
    }
    if ( $EMAIL_K eq '' ) {
        push(@USER_ERROR_LIST,$EMAIL_K_V,$ERROR_MSG1,$DUMMY);
        $err_f = 1;
        $mail_err = 1;
    }
    if ( $EMAIL ne '' ) {
        if ( &mail_check($EMAIL) != 0) {
            push(@USER_ERROR_LIST,$EMAIL_V,$ERROR_MSG2,$DUMMY);
            $err_f = 1;
            $mail_err = 1;
        }
    }
    if ( $EMAIL_K ne '' ) {
        if ( &mail_check($EMAIL_K) != 0) {
            push(@USER_ERROR_LIST,$EMAIL_K_V,$ERROR_MSG2,$DUMMY);
            $err_f = 1;
            $mail_err = 1;
        }
    }
    if ( $mail_err == 0 ) {
        if ( $EMAIL ne $EMAIL_K ) {
            push(@USER_ERROR_LIST,$EMAIL_V,$ERROR_MSG3,$DUMMY);
            $err_f = 1;
        }
    }

}
## -------------------------------------------------------------------
## メールアドレスチェックサブルーチン
## ------------------------------------------------------------------- 
sub mail_check {
    my $address = shift;
    my $status = 0;

    $address=~ s/^\s+//;
    $address=~ s/\s+$//;

    if ($address =~ /.\@(.+)$/i) {
        my $domain = $1;
        if ($domain =~ /\.[^a-z]+$/) {
            $status = 1;
        }
        if ($domain =~ /[\e\255-\377]/) {
            $status = 1;
        }
        if ($domain =~ /[^a-z0-9.-]/i) {
            $status = 1;
        }
    }
    else {
        $status = 1;
    }

    return $status;
}
## -------------------------------------------------------------------
## 確認項目セットサブルーチン
## -------------------------------------------------------------------
sub kakunin_set {
    push(@USER_VALUE_LIST,$NAME_K_V,$NAME_K1 . ' ' . $NAME_K2,'name_k');
    push(@USER_VALUE_LIST,$NAME_F_V,$NAME_F1 . ' ' . $NAME_F2,'name_f');
    push(@USER_VALUE_LIST,$KNAME_K_V,$KNAME_K1 . ' ' . $KNAME_K2,'kname_k');
    push(@USER_VALUE_LIST,$KNAME_F_V,$KNAME_F1 . ' ' . $KNAME_F2,'kname_f');
    push(@USER_VALUE_LIST,$BIRTH_YMD_V,$BIRTH_YYYY . '/' . $BIRTH_MM . '/' . $BIRTH_DD ,'birth_ymd');
    push(@USER_VALUE_LIST,$SEX_V,$SEX,'sex');
    push(@USER_VALUE_LIST,$YUBIN_V,$YUBIN ,'yubin');
    push(@USER_VALUE_LIST,$PREF_V,$PREF,'pref');
    push(@USER_VALUE_LIST,$ADDR_V,$ADDR1,'addr1');
    push(@USER_VALUE_LIST,' ',$ADDR2,'addr2');
    push(@USER_VALUE_LIST,$TEL_V,$TEL,'tel');
    push(@USER_VALUE_LIST,$EMAIL_V,$EMAIL,'email');
}
## -------------------------------------------------------------------
## ユーザーメッセージ出力サブルーチン
## -------------------------------------------------------------------
sub user_message {
    my $file = shift;
    my @rarray_list = @_;
#print "file：$file<br>\n";
    open(INPUT,"<$file") or die "$file オープンエラー";

#    print "$CONTENT_TYPE\r\n\r\n";

  TemplateBlock: {
    OutputTemplate:
      while (<INPUT>) {
          if (/<!--/ && /-->/) {
              if (/\bRoopFormat\b/i &&
                  /\bStart\b/i)
              {
                  last OutputTemplate;

              }
          }
          print ;
      }

      my $roop_form = '';
    ReadFormat:
      while (<INPUT>) {
          if (/<!--/ && /-->/) {
              if (/\bRoopFormat\b/i &&
                  /\bEnd\b/i)
              {
                  my $rarray = shift @rarray_list;
                  my @in = @$rarray;

                  while (@in) {
                      my $c1 = shift @in;
                      my $c2 = shift @in;
                      my $c3 = shift @in;

                      my $form_x = $roop_form;
                      $form_x =~ s/%s1/$c1/g;
                      $form_x =~ s/%s2/$c2/g;
                      $form_x =~ s/%s3/$c3/g;

                      print "$form_x\n";
                  }

                  redo TemplateBlock;
             }
          }

          $roop_form .= $_;

      }
    }
    close(INPUT);
}

## -------------------------------------------------------------------
## メール出力サブルーチン
## -------------------------------------------------------------------
sub output_mail {
    my @words = @_;

#    $COMMAND_SENDER  = "| $nkf -jm0 | $sendmail -t -f$mail_to";
    $COMMAND_SENDER  = "| $sendmail -t";

##  ヘッダー編集
my $HEAD=<<EOF;
From: $EMAIL
To: $mail_to
Subject: Baby Photo Gallery
Content-Type: Text/Plain; charset=iso-2022-jp

EOF
    &jcode'convert(*HEAD,'jis');

##  本文作成
    open(MAIL,$COMMAND_SENDER);
    print MAIL $HEAD;

#    &jcode'convert(*MSG1,'jis');
#    print MAIL $MSG1;

my $MSG1=<<EOF;
-------------------------------
【我が家の天使応募内容】
-------------------------------
お名前          ：$NAME_K
フリガナ        ：$NAME_F
お子様のお名前　：$KNAME_K
お子様のフリガナ：$KNAME_F
お子様の生年月日：$BIRTH_YMD
お子様の性別　　：$SEX
郵便番号　 　　 ：$YUBIN
都道府県　　　  ：$PREF
住所　 　　　　 ：$ADDR1 $ADDR2
TEL 　　　    　：$TEL
E-mail　　      ：$EMAIL
EOF
    &jcode'convert(*MSG1,'jis');
    print MAIL $MSG1;

#    my $MSG2 = '';
#    while (@words) {
#        my $key = shift @words;
#        my $val = shift @words;
#        my $dmy = shift @words;
#
#        $MSG2 .= printf MAIL $DATA_FORM, $key, $val;
#        $MSG2 .= "$key ： $val\n";
#    }
#    &jcode'convert(*MSG2,'jis');
#    print MAIL $MSG2;    

    close(MAIL);

}