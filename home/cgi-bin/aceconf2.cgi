## -------------------------------------------------------------------
$MKNAME  = "株式会社エース";			# v1.2 2000/12/01 
$MKURL   = "http://www.ace-ace.co.jp/";
## -------------------------------------------------------------------
# 
$THANKS_PAGE  = '../home/angel_formthx.htm';               # 完了画面
$CONFIRM_PAGE = '../home/angel_formchk.htm';               # 確認画面
$FORMERR_PAGE = '../home/angel_formerr.htm';               # エラー画面

$NAME_K_V     = q( お名前 );	# 漢字氏名
$NAME_F_V     = q( フリガナ );	# フリガナ氏名
$KNAME_K_V     = q( お子様のお名前 );	# 子供漢字氏名
$KNAME_F_V     = q( お子様のフリガナ );	# 子供フリガナ氏名

$BIRTH_YMD_V        = q( お子様の生年月日 );	        # 生年月日

$SEX_V        = q( お子様の性別 );	        # 性別

$ADDR_V        = q( 住所 );	        # 住所
$YUBIN_V        = q( 郵便番号 );        # 郵便番号
$PREF_V        = q( 都道府県 );	        # 都道府県
$ADDR1_V        = q( 住所１ );	        # 住所１
$ADDR2_V        = q( 住所２);	        # 住所２

$TEL_V        = q( TEL );	        # 電話番号
$EMAIL_V      = q( E-mail );	# メールアドレス
$EMAIL_K_V    = q( E-mail（確認） );	# メールアドレス（確認用）

$ERROR_MSG1 = q( 入力が必須の項目に入力が正しく行われていません。 );
$ERROR_MSG2 = q( メールアドレスが正しく入力されていません。 );
$ERROR_MSG3 = q( メールアドレスが確認用メールアドレスと違います。 );

# 各ファイル名の設定
#$LOCKFILE= 'data.loc';				# ロックファイル

## 初期値の設定
$COMMENT_KEY      = '項目名';
$COMMENT_VALUE    = '入力内容';
$MAX_FORM_NAME      = 20;
$DATA_FORM_SEPRETER = '  ';
$DATA_FORM = "%-${MAX_FORM_NAME}s${DATA_FORM_SEPRETER}%s\n";
$DUMMY = '';

## テーブル設定
@DUMMY_LIST = '';

## メールの設定
$sendmail = '/usr/lib/sendmail';                #sendmailへのパスを指定
#$data_log = './data.cgi';                               #CSVファイルへのパスを指定(空のファイルdata.cgi[606]を作成)
#$home = 'http://www.ace-ace.co.jp/';                 #メール送信後に戻るページのURLを指定
#$mail_to = 'info@shashinkan-teddybear.co.jp';                #メール送信先アドレスを指定
$mail_to = 'chie.m@ace-ace.co.jp';  
$mail_sub = 'お問合せ';

## --- 基本設定 ここまで

## -------------------------------------------------------------------
## パッケージ・ライブラリ宣言
## -------------------------------------------------------------------
package Com;
## -------------------------------------------------------------------
## 引数解析
## -------------------------------------------------------------------
sub getagv {
	if ( $ENV{'CONTENT_TYPE'}  =~ m!^multipart/form-data!) { return 1; }
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	} else {
		$buffer = $ENV{'QUERY_STRING'};
	}
	@pairs = split(/&/, $buffer);
	foreach $pair (@pairs) {
		($name, $value) = split(/=/, $pair);
		$value =~ tr/+/ /;
		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		$value =~ s/<!--(.|\n)*-->//g;	#SSI 	除去(セキュリティ対策)
		$value =~ s/applet//gi;		#Java		除去(セキュリティ対策)
		$value =~ s/<script//gi;		#Javascript	除去(セキュリティ対策)
		$value =~ s/<META(.+)Refresh(.*)>(\s*)(\n?)//ig; #METAタグ飛ばし禁止
		$value =~ s/<(.*)style(\s*)=(.|\n)*>//ig;        #Styleタグ禁止
		&jcode'convert(*value, 'sjis');
		$FORM{$name} = $value;
#                print "$name = $value<br>\n";
	}
	return 0;
}
## -------------------------------------------------------------------
## ロック処理サブルーチン
## -------------------------------------------------------------------
sub lockon {
	local($lfile) = @_;
	$LCKKEY = 0;
	foreach (1 .. 5) {
		unless (-e "$lfile") {
			if ( open(LOCK,">$lfile") ) {
				close(LOCK);
				$LCKKEY = 9;
			}
			last;
		} else { sleep(1); }
	}
	if ($LCKKEY == 0) { return 0; }
	return 9;
}
## -------------------------------------------------------------------
## アンロック処理サブルーチン
## -------------------------------------------------------------------
sub lockoff {
	local($lfile,$key) = @_;
	if ($key == 9) { unlink("$lfile"); }
}
1;
